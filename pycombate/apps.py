from django.apps import AppConfig


class PycombateConfig(AppConfig):
    name = 'pycombate'
