from django.conf.urls import url
from pycombate.views import home

urlpatterns = [
    url(r'^$', home)
]
