from django.contrib import admin
from pycombate.models import Banner, Fight, TvProvider

# Register your models here.
#python manage.py makemigrations
#python manage.py migrate
admin.site.register(Banner)
admin.site.register(Fight)
admin.site.register(TvProvider)
