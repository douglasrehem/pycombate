from django.shortcuts import render
from pycombate.models import Banner, Fight, TvProvider

# Create your views here.
def home(request):
    banners = Banner.objects.all().order_by('id')
    fights = Fight.objects.all().order_by('date')
    providers = TvProvider.objects.all().order_by('id')
    return render(request, 'pycombate/index.html', {'banners' : banners, 'fights' : fights, 'providers' : providers})
