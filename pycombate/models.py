from django.db import models

# Create your models here.
class Banner(models.Model):
    title = models.CharField(max_length = 100)
    image = models.FileField(upload_to='documents/banners/%Y/%m/%d')

class Fight(models.Model):
    title = models.CharField(max_length = 100)
    first_fighter = models.CharField(max_length = 50)
    second_fighter = models.CharField(max_length = 50)
    date = models.DateField()
    image = models.FileField(upload_to='documents/fights/%Y/%m/%d')

class TvProvider(models.Model):
    name = models.CharField(max_length = 100)
    image = models.FileField(upload_to='documents/tv-providers/%Y/%m/%d')
