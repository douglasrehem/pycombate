**Observações**

Eu comitei o banco do sqlite para que a página seja visualizada sem ter que adicionar nenhum conteúdo previamente.

http://localhost:8000/admin
Username: admin
password: pycombate
---

## Instalar dependências e compilar CSS
1. na pasta raíz executar **bower install**
2. na pasta raíz executar **sass --update pycombate/static/scss:pycombate/static/css**

---
